#!/bin/bash

cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat system/system/priv-app/FlymeGallery/FlymeGallery.apk.* 2>/dev/null >> system/system/priv-app/FlymeGallery/FlymeGallery.apk
rm -f system/system/priv-app/FlymeGallery/FlymeGallery.apk.* 2>/dev/null
cat system/system/priv-app/VoiceAssistant/VoiceAssistant.apk.* 2>/dev/null >> system/system/priv-app/VoiceAssistant/VoiceAssistant.apk
rm -f system/system/priv-app/VoiceAssistant/VoiceAssistant.apk.* 2>/dev/null
cat system/system/priv-app/DirectService/DirectService.apk.* 2>/dev/null >> system/system/priv-app/DirectService/DirectService.apk
rm -f system/system/priv-app/DirectService/DirectService.apk.* 2>/dev/null
cat system/system/priv-app/FlymeMusic/FlymeMusic.apk.* 2>/dev/null >> system/system/priv-app/FlymeMusic/FlymeMusic.apk
rm -f system/system/priv-app/FlymeMusic/FlymeMusic.apk.* 2>/dev/null
cat system/system/priv-app/Browser/Browser.apk.* 2>/dev/null >> system/system/priv-app/Browser/Browser.apk
rm -f system/system/priv-app/Browser/Browser.apk.* 2>/dev/null
cat system/system/priv-app/Video/Video.apk.* 2>/dev/null >> system/system/priv-app/Video/Video.apk
rm -f system/system/priv-app/Video/Video.apk.* 2>/dev/null
cat modem/image/sdx55m/qdsp6sw.mbn.* 2>/dev/null >> modem/image/sdx55m/qdsp6sw.mbn
rm -f modem/image/sdx55m/qdsp6sw.mbn.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/baidu/baidu.apk.* 2>/dev/null >> product/custom/3rd-party/apk/baidu/baidu.apk
rm -f product/custom/3rd-party/apk/baidu/baidu.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/BaiduNetDisk/BaiduNetDisk.apk.* 2>/dev/null >> product/custom/3rd-party/apk/BaiduNetDisk/BaiduNetDisk.apk
rm -f product/custom/3rd-party/apk/BaiduNetDisk/BaiduNetDisk.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/app/SnapdragonSVA/SnapdragonSVA.apk.* 2>/dev/null >> system_ext/app/SnapdragonSVA/SnapdragonSVA.apk
rm -f system_ext/app/SnapdragonSVA/SnapdragonSVA.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
