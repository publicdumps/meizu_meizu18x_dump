## qssi-user 11 RKQ1.210715.001 1627442563 release-keys
- Manufacturer: meizu
- Platform: kona
- Codename: meizu18X
- Brand: meizu
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.210715.001
- Incremental: 1627442563
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: meizu/meizu_18X_CN/meizu18X:11/RKQ1.210408.001/1627442563:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.210715.001-1627442563-release-keys
- Repo: meizu_meizu18x_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
